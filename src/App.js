import React from 'react';
import './App.css';

class App extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      show: true,
      showH1: true,
      showDiv: true,
      showH2: true,
      showH3: true,
      showB: true,
    }
  }

  hideButton = () => {
    this.setState({ show: false })
  }

  showButton = () => {
    this.setState({ show: true })
  }

  esconderH1 = () => {
    this.setState({ showH1: false })
  }

  mostrarH1 = () => {
    this.setState({ showH1: true })
  }

  esconderDiv = () => {
    this.setState({ showDiv: false })
  }

  mostrarDiv = () => {
    this.setState({ showDiv: true })
  }

  esconderH2 = () => {
    this.setState({ showH2: false })
  }

  mostrarH2 = () => {
    this.setState({ showH2: true })
  }


  changeH3 = () => {
    this.setState({showH3: !this.state.showH3 })
  }


  changeB = () => {
    this.setState({showB: !this.state.showB })
  }

  render() {
    return (
      <div>

        <button onClick={this.hideButton}> Esconda Div!
        </button>
        <button onClick={this.showButton}> Mostre a Div!
        </button>

        {
          this.state.show &&
          <p>
            Olá, React!
          </p>
        }

        <div>

          <button onClick={this.esconderH1}> Esconda o H1! </button>
          <button onClick={this.mostrarH1}> Mostre o H1! </button>

          {
            this.state.showH1 &&

            <h1> Olá, h1! </h1>
          }

          <button onClick={this.esconderDiv}> Esconda a Div </button>
          <button onClick={this.mostrarDiv}> Mostre a Div </button>

          {this.state.showDiv &&
            <div>

<p>              Olá, Div!
</p>
          </div>
          }

          <button onClick={this.esconderH2}> Esconda o H2! </button>
          <button onClick={this.mostrarH2}> Mostre o H2! </button>

          {
            this.state.showH2 &&

            <h2> Olá, h2! </h2>
          }
      <div>

          <button onClick={this.changeH3}> Mostra/Esconde H3 </button>

            {this.state.showH3 &&

            <h3> Olá, h3! </h3>
             
            }
       

       <button onClick={this.changeB}> Mostra/Esconde B </button>

       <div>
{this.state.showB &&

<p> 
<b> Olá, Bold! </b>
 </p>
}

</div>
</div> 

</div> 
</div> 

    );
  }
}

export default App;
